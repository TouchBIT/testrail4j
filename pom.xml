<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ MIT License
  ~
  ~ Copyright © 2019 TouchBIT.
  ~ Copyright © 2019 Oleg Shaburov.
  ~ Copyright © 2018 Maria Vasilenko.
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.sonatype.oss</groupId>
        <artifactId>oss-parent</artifactId>
        <version>7</version>
    </parent>

    <groupId>org.touchbit.testrail4j</groupId>
    <artifactId>parent</artifactId>
    <version>2.1.2</version>
    <packaging>pom</packaging>

    <name>TestRail4J parent</name>
    <url>https://gitlab.com/TouchBIT/testrail4j</url>
    <description>Parent pom for TestRail4J modules</description>

    <issueManagement>
        <url>https://gitlab.com/TouchBIT/testrail4j/issues</url>
        <system>GitLab</system>
    </issueManagement>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>https://choosealicense.com/licenses/mit/</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:git@gitlab.com:TouchBIT/testrail4j.git</connection>
        <developerConnection>scm:git:git@gitlab.com:TouchBIT/testrail4j.git</developerConnection>
        <url>https://gitlab.com/TouchBIT/testrail4j</url>
    </scm>

    <developers>
        <developer>
            <id>oleg.shaburov</id>
            <name>Oleg Shaburov</name>
            <email>oleg.shaburov@touchbit.org</email>
            <roles>
                <role>Owner</role>
            </roles>
            <timezone>+3</timezone>
        </developer>
        <developer>
            <id>maria.vasilenko</id>
            <name>Maria Vasilenko</name>
            <email>maria.vasilenko@touchbit.org</email>
            <roles>
                <role>Developer</role>
            </roles>
            <timezone>+3</timezone>
        </developer>
    </developers>

    <modules>
        <module>testrail4j-integration-tests</module>
        <module>testrail4j-test-core</module>
        <module>testrail4j-schema</module>
        <module>testrail4j-core</module>
        <module>gson-feign-client</module>
        <module>jackson2-feign-client</module>
        <module>gson-api-model</module>
        <module>jackson2-api-model</module>
    </modules>

    <properties>
        <jdk.version>1.8</jdk.version>
        <encoding>UTF-8</encoding>
        <maven.compiler.source>${jdk.version}</maven.compiler.source>
        <maven.compiler.target>${jdk.version}</maven.compiler.target>
        <file.encoding>${encoding}</file.encoding>
        <source.encoding>${encoding}</source.encoding>
        <project.build.sourceEncoding>${encoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${encoding}</project.reporting.outputEncoding>

        <junit.report>${project.build.directory}/report/junit-report</junit.report>
        <jacoco.report>${project.build.directory}/report/jacoco.exec</jacoco.report>

        <sonar.gitlab.project_id>8282105</sonar.gitlab.project_id>
        <sonar.sources>src/main</sonar.sources>
        <sonar.java.binaries>target/classes</sonar.java.binaries>
        <sonar.java.test.binaries>target/test-classes</sonar.java.test.binaries>
        <sonar.host.url>https://touchbit.org/sonar/</sonar.host.url>
        <sonar.projectKey>${project.groupId}:testrail4j</sonar.projectKey>
        <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
        <reports.path>${project.build.directory}/reports</reports.path>
        <sonar.junit.reportPaths>${reports.path}</sonar.junit.reportPaths>
        <sonar.jacoco.reportPaths>${reports.path}/jacoco.exec</sonar.jacoco.reportPaths>
        <sonar.skippedModules>integration, model4jackson2, model4gson</sonar.skippedModules>
        <testrail4j.sonar.exclusions>
            *.xml,
            src/main/java/org/touchbit/testrail4j/**/model/*,
            src/main/java/org/touchbit/testrail4j/**/model/**/*
        </testrail4j.sonar.exclusions>
        <sonar.exclusions>${testrail4j.sonar.exclusions}</sonar.exclusions>
        <sonar.coverage.exclusions>${testrail4j.sonar.exclusions}</sonar.coverage.exclusions>
        <sonar.cpd.exclusions>src/main/java/org/touchbit/testrail4j/**/feign/client/*</sonar.cpd.exclusions>
        <model.annotation.lib>none</model.annotation.lib>

        <gson.version>2.8.5</gson.version>
        <feign.version>10.1.0</feign.version>
        <jackson.version>2.9.7</jackson.version>
        <junit.jupiter.version>5.3.1</junit.jupiter.version>
        <commons.lang3.version>3.8.1</commons.lang3.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- Self dependency management -->
            <dependency>
                <groupId>org.touchbit.testrail4j</groupId>
                <artifactId>testrail4j-test-core</artifactId>
                <version>2.1.2</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.touchbit.testrail4j</groupId>
                <artifactId>testrail4j-core</artifactId>
                <version>2.1.2</version>
            </dependency>
            <dependency>
                <groupId>org.touchbit.testrail4j</groupId>
                <artifactId>jackson2-api-model</artifactId>
                <version>2.1.2</version>
            </dependency>
            <dependency>
                <groupId>org.touchbit.testrail4j</groupId>
                <artifactId>jackson2-feign-client</artifactId>
                <version>2.1.2</version>
            </dependency>
            <dependency>
                <groupId>org.touchbit.testrail4j</groupId>
                <artifactId>gson-api-model</artifactId>
                <version>2.1.2</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/io.github.openfeign -->
            <dependency>
                <groupId>io.github.openfeign</groupId>
                <artifactId>feign-core</artifactId>
                <version>${feign.version}</version>
            </dependency>
            <dependency>
                <groupId>io.github.openfeign</groupId>
                <artifactId>feign-jackson</artifactId>
                <version>${feign.version}</version>
            </dependency>
            <dependency>
                <groupId>io.github.openfeign</groupId>
                <artifactId>feign-gson</artifactId>
                <version>${feign.version}</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>${gson.version}</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons.lang3.version}</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.7.25</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.0</version>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>2.5</version>
                    <configuration>
                        <generateBackupPoms>false</generateBackupPoms>
                        <allowIncrementalUpdates>true</allowIncrementalUpdates>
                        <processAllModules>true</processAllModules>
                        <updateMatchingVersions>true</updateMatchingVersions>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.22.0</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.junit.platform</groupId>
                            <artifactId>junit-platform-surefire-provider</artifactId>
                            <version>1.3.1</version>
                        </dependency>
                        <dependency>
                            <groupId>org.junit.jupiter</groupId>
                            <artifactId>junit-jupiter-engine</artifactId>
                            <version>${junit.jupiter.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <useSystemClassLoader>true</useSystemClassLoader>
                        <reportsDirectory>${sonar.junit.reportPaths}</reportsDirectory>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>0.8.1</version>
                    <executions>
                        <execution>
                            <id>pre-unit-test</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                            <configuration>
                                <destFile>${sonar.jacoco.reportPaths}</destFile>
                            </configuration>
                        </execution>
                        <execution>
                            <id>report</id>
                            <phase>prepare-package</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                            <configuration>
                                <dataFile>${sonar.jacoco.reportPaths}</dataFile>
                                <outputDirectory>${reports.path}/jacoco</outputDirectory>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                    <executions>
                        <execution>
                            <phase>compile</phase>
                        </execution>
                    </executions>
                    <configuration>
                        <source>${jdk.version}</source>
                        <target>${jdk.version}</target>
                        <encoding>${source.encoding}</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jsonschema2pojo</groupId>
                    <artifactId>jsonschema2pojo-maven-plugin</artifactId>
                    <version>0.5.1</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>generate</goal>
                            </goals>
                            <configuration>
                                <!--suppress UnresolvedMavenProperty -->
                                <sourceDirectory>
                                    ${project.parent.basedir}/testrail4j-schema/src/main/resources/schema
                                </sourceDirectory>
                                <outputDirectory>src/main/java</outputDirectory>
                                <targetPackage>org.touchbit.testrail4j.${model.annotation.lib}.model</targetPackage>
                                <useCommonsLang3>true</useCommonsLang3>
                                <generateBuilders>true</generateBuilders>
                                <includeConstructors>true</includeConstructors>
                                <annotationStyle>${model.annotation.lib}</annotationStyle>
                                <removeOldOutput>true</removeOldOutput>
                                <useLongIntegers>true</useLongIntegers>
                                <includeAdditionalProperties>true</includeAdditionalProperties>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>3.1.1</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>tree</goal>
                            </goals>
                            <phase>generate-resources</phase>
                            <configuration>
                                <scope>compile</scope>
                                <outputFile>${project.build.directory}/info/${project.artifactId}.txt</outputFile>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.1.0</version>
                    <executions>
                        <execution>
                            <phase>process-resources</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.parent.basedir}/docs/dependencies/</outputDirectory>
                                <resources>
                                    <resource>
                                        <directory>${project.build.directory}/info/</directory>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>3.0.1</version>
                    <executions>
                        <execution>
                            <phase>install</phase>
                        </execution>
                        <execution>
                            <id>attach-sources</id>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>2.5.2</version>
                    <executions>
                        <execution>
                            <phase>install</phase>
                            <id>install-jar</id>
                            <configuration>
                                <file>target/${project.artifactId}-${project.version}.jar</file>
                                <sources>target/${project.artifactId}-${project.version}-sources.jar</sources>
                                <pomFile>dependency-reduced-pom.xml</pomFile>
                                <updateReleaseInfo>true</updateReleaseInfo>
                                <createChecksum>true</createChecksum>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>3.1.0</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                            <phase>deploy</phase>
                            <configuration>
                                <failOnError>false</failOnError>
                                <failOnWarnings>false</failOnWarnings>
                                <detectOfflineLinks>false</detectOfflineLinks>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>1.6</version>
                    <configuration>
                        <passphrase/>
                    </configuration>
                    <executions>
                        <execution>
                            <id>sign-artifacts</id>
                            <phase>deploy</phase>
                            <goals>
                                <goal>sign</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.sonatype.plugins</groupId>
                    <artifactId>nexus-staging-maven-plugin</artifactId>
                    <version>1.6.8</version>
                    <extensions>true</extensions>
                    <configuration>
                        <serverId>sonatype</serverId>
                        <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                        <updateReleaseInfo>true</updateReleaseInfo>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <configuration>
                        <skip>true</skip>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <distributionManagement>
        <snapshotRepository>
            <id>sonatype</id>
            <name>Nexus Snapshot Repository</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>sonatype</id>
            <name>Nexus Release Repository</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
    <repositories>
        <repository>
            <id>touchbit.org.repository</id>
            <url>https://touchbit.org/repository/</url>
        </repository>
    </repositories>
</project>