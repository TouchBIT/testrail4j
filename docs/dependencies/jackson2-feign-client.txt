org.touchbit.testrail4j:jackson2-feign-client:jar:2.1.2
+- org.touchbit.testrail4j:testrail4j-core:jar:2.1.2:compile
|  \- org.slf4j:slf4j-api:jar:1.7.25:compile
+- org.touchbit.testrail4j:jackson2-api-model:jar:2.1.2:compile
|  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.9.7:compile
|  \- org.apache.commons:commons-lang3:jar:3.8.1:compile
+- io.github.openfeign:feign-core:jar:10.1.0:compile
\- io.github.openfeign:feign-jackson:jar:10.1.0:compile
   \- com.fasterxml.jackson.core:jackson-databind:jar:2.9.6:compile
      \- com.fasterxml.jackson.core:jackson-core:jar:2.9.6:compile
